#include <stdio.h>
#include <assert.h>
#include "hasard.h"

int main() {
    char c = hasard();
    assert((c == 'R') || (c == 'P') || (c == 'C'));
    printf("%c\n", c);
    return 0;
} 
