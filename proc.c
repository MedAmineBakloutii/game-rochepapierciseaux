#include "hasard.h"
#include "comparison.h"
#include <stdio.h>

int main() {
    char playerChoice;
    char computerChoice;

    printf("Choose R (rock), P (paper), or C (scissors): ");
    scanf("%c", &playerChoice);

    if (playerChoice != 'R' && playerChoice != 'P' && playerChoice != 'C') {
        printf("Invalid choice. Please choose R, P, or C.\n");
        return 1;
    }

    computerChoice = hasard();
    int result = comparison(playerChoice, computerChoice);

    switch (result) {
        case 1:
            printf("Player wins!\n");
            break;
        case -1:
            printf("Computer wins!\n");
            break;
        default:
            printf("It's a tie!\n");
            break;
    }

    return 0;
}
