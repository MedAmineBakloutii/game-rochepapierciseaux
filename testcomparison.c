#include <assert.h>
#include "comparison.h"

int main() {
    assert(comparison('R', 'R') == 0);
    assert(comparison('P', 'P') == 0);
    assert(comparison('C', 'C') == 0);
    
    assert(comparison('R', 'C') == 1);
    assert(comparison('P', 'R') == 1);
    assert(comparison('C', 'P') == 1);
    
    assert(comparison('R', 'P') == -1);
    assert(comparison('P', 'C') == -1);
    assert(comparison('C', 'R') == -1);
    
    return 0;
}

