#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hasard.h"

// Function to obtain a random choice for the computer (R, P, or C)
char hasard() {
    srand(time(NULL));
    int randomChoice = rand() % 3;
    
    if (randomChoice == 0) {
        return 'R';
    } else if (randomChoice == 1) {
        return 'P';
    } else {
        return 'C';
    }
}
