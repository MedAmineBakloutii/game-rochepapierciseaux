#include <stdio.h>
#include <assert.h>
#include "hasard.h"
#include "comparison.h"

int main(int argc, char** argv) {
    int i, result;
    char computerChoice;

    for (i = 1; i < argc; i++) {
        computerChoice = hasard();
        result = comparison(argv[i][0], computerChoice);
        printf("Player: %c\tComputer: %c\tResult: %d\n", argv[i][0], computerChoice, result);
    }

    return 0;
}
