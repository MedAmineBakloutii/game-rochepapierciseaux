#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "comparison.h"

// Function to compare the choices of the player and the computer
int comparison(char playerChoice, char computerChoice) {
    if (playerChoice == computerChoice) {
        return 0; // Tie
    } else if ((playerChoice == 'R' && computerChoice == 'C') ||
               (playerChoice == 'P' && computerChoice == 'R') ||
               (playerChoice == 'C' && computerChoice == 'P')) {
        return 1; // Player wins
    } else {
        return -1; // Computer wins
    }
}

