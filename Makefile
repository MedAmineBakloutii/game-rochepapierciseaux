CC=gcc
CFLAGS=-std=c99 -Wall


TESTFLAGS=-fprofile-arcs -ftest-coverage

clean:
	rm -f *.o testhasard testcomparison testapp *.gcov *.gcda *.gcno

testcomparison: testcomparison.c comparison.h comparison.c
	# the comparison test
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testcomparison.c comparison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testcomparison testcomparison.o comparison.o
	# Run the test
	./testcomparison
	gcov -cp testcomparison


testhasard: testhasard.c hasard.h hasard.c
	# the hasard test
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testhasard.c hasard.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.o hasard.o
	# run the hasard test
	./testhasard
	gcov -cp testhasard

testapp: testapp.c hasard.h comparison.h hasard.c comparison.c
	# the testapp
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testapp.c hasard.c comparison.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testapp testapp.o hasard.o comparison.o
	# Run the testapp
	./testapp R PCRRCCPP
	gcov -cp testapp
